## Abandoned Flat Changelog

#### Version 2.2

* Reverted the move of Abilities to Curses for better compatibility with the original engine. The curse changes will be provided by [NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/readme/#ncgdmw-patches).
* Added a compatibility patch to move the Abandoned Flat just a ways to the North when playing with [OAAB Brother Junipers Twin Lamps](https://www.nexusmods.com/morrowind/mods/51424).

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat/-/packages/20991592) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53141)

#### Version 2.1

* Cleaned dirty records
* Fixed broken scripts (where possible)
* Reimplemented buff spells as "Curses" instead of "Abilities"

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat/-/packages/15713834) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53141)

#### Version 2.0

Original classic release of the mod.

[Download Link](https://modding-openmw.com/files/Abandoned_Flatv2.zip)
