#!/bin/sh
set -e

delta_plugin convert Abandoned_Flat.yaml
mv Abandoned_Flat.omwaddon Abandoned_Flat.esp

delta_plugin convert AbandonedFlat+OAABBrotherJunipersTwinLamps.yaml
mv AbandonedFlat+OAABBrotherJunipersTwinLamps.omwaddon AbandonedFlat+OAABBrotherJunipersTwinLamps.esp
