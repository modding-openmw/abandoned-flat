# Abandoned Flat

This project aims to preserve the classic player home mod [Abandoned Flat](http://www.ghostwhowalks.net/mods.htm) by Ghost Who Walks.

Includes an optional patch to move the Abandoned Flat when playing with [OAAB Brother Junipers Twin Lamps](https://www.nexusmods.com/morrowind/mods/51424).

#### Credits

* **Author**: [Ghost Who Walks](http://www.ghostwhowalks.net/)
* **Edits by**: johnnyhostile

**Special Thanks**:

* [Ghost Who Walks](http://www.ghostwhowalks.net/) for making this mod
* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Changes

* Cleaned dirty records
* Fixed broken scripts (where possible)

#### Web

* [Project Home](https://modding-openmw.gitlab.io/abandoned-flat/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53141)
* [Source on GitLab](https://gitlab.com/modding-openmw/abandoned-flat)

#### Installation

1. Download the zip from a link above
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\PlayerHomes\abandoned-flat

        # Linux
        /home/username/games/OpenMWMods/PlayerHomes/abandoned-flat

        # macOS
        /Users/username/games/OpenMWMods/PlayerHomes/abandoned-flat
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\PlayerHomes\abandoned-flat"
1. Enable the mod plugins via OpenMW-Launcher ([video](https://www.youtube.com/watch?v=xzq_ksVuRgc)), or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=Abandoned_Flat.esp
1. Enjoy!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/abandoned-flat/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/53141?tab=posts)
